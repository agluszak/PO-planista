package pl.edu.mimuw.ag385527;

/**
 * Klasa reprezentująca opis procesu wczytany z wejścia
 */
public class ProcessDescription {
    private final int id;
    private final int start;
    private final int requestedTime;

    public ProcessDescription(int id, int start, int requestedTime) {
        this.id = id;
        this.start = start;
        this.requestedTime = requestedTime;
    }

    public int getId() {
        return id;
    }

    public int getStart() {
        return start;
    }

    public int getRequestedTime() {
        return requestedTime;
    }

}
