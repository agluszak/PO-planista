package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.LinkedList;
import java.util.List;

public class RobinRound extends SingleProcessStrategy {
    private static final String BASE_NAME = "RR-";
    private final String name;
    private final int quantum;
    private double partOfQuantumDone;

    public RobinRound(List<ProcessDescription> processesToRun, int quantum) {
        super(processesToRun, new LinkedList<>());
        name = BASE_NAME + String.valueOf(quantum);
        this.quantum = quantum;
        partOfQuantumDone = 0;
    }

    @Override
    protected void handleCurrentProcess() {
        double timeUntilNextEvent;
        double timeUntilCurrentProcessIsDone = getCurrentProcess().getRemainingTime();
        if (getNextProcess() != null) {
            double timeUntilNextProcessAppears = getNextProcess().getStartTime() - getCurrentTime();
            timeUntilNextEvent = Math.min(timeUntilCurrentProcessIsDone, timeUntilNextProcessAppears);
        } else {
            timeUntilNextEvent = timeUntilCurrentProcessIsDone;
        }
        double timeUntilNextEventOrQuantumEnd = Math.min(timeUntilNextEvent, quantum - partOfQuantumDone);

        partOfQuantumDone += timeUntilNextEventOrQuantumEnd;
        advanceTime(timeUntilNextEventOrQuantumEnd);
        getCurrentProcess().runFor(timeUntilNextEventOrQuantumEnd, getCurrentTime(), timeUntilNextEventOrQuantumEnd);

        if (getCurrentProcess().isFinished()) {
            addToFinished(getCurrentProcess());
            setCurrentProcess(null);
            partOfQuantumDone = 0;
        } else {
            if (partOfQuantumDone == quantum) {
                addProcessToQueue(getCurrentProcess());
                setCurrentProcess(null);
                partOfQuantumDone = 0;
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
