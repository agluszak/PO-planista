package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;
import pl.edu.mimuw.ag385527.stats.Stat;
import pl.edu.mimuw.ag385527.stats.StatData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Klasa zawierająca pewne domyślne pola i metody z których korzystają implementacje strategii
 * wymagane w zadaniu, a które jednak w ogólności nie są wymagane.
 * Symulacji działania strategii nie przeprowadzamy tick po ticku, gdyż byłoby to niewydajne.
 * Zamiast tego symulacja przeskakuje jedynie między interesującymi momentami w czasie
 * (np. zakończenie się procesu, rozpoczęcie się kolejnego, w przypadku RR - koniec kwantu czasu)
 */
public abstract class AbstractStrategy implements Strategy {
    private final List<Process> finished;
    private final Queue<Process> futureProcesses;
    private double currentTime;

    public AbstractStrategy(List<ProcessDescription> processesToRun) {
        List<Process> futureProcessesList = processesToRun.stream()
                .map(Process::new).sorted(Process.DEFAULT_COMPARATOR).collect(Collectors.toList());
        futureProcesses = new LinkedList<>();
        futureProcesses.addAll(futureProcessesList);
        finished = new ArrayList<>();
        currentTime = 0;
    }

    public void run() {
        while (anyProcessLeft()) {
            addProcessesToQueue();
            handleProcesses();
            skipWaitingTime();
        }
    }

    protected abstract boolean anyProcessLeft();

    protected abstract void addProcessesToQueue();

    protected abstract void skipWaitingTime();

    protected abstract void handleProcesses();

    public <T> T getStat(Stat<T> stat) {
        List<Integer> processIDs = new ArrayList<>();
        Map<Integer, Integer> startTimes = new TreeMap<>();
        Map<Integer, Double> finishTimes = new TreeMap<>();
        Map<Integer, Double> runTimes = new TreeMap<>();
        Map<Integer, Integer> requestedTimes = new TreeMap<>();
        finished.forEach(p -> {
                    processIDs.add(p.getId());
                    startTimes.put(p.getId(), p.getStartTime());
                    finishTimes.put(p.getId(), p.getFinishTime());
                    runTimes.put(p.getId(), p.getTimeInProcessing());
                    requestedTimes.put(p.getId(), p.getRequestedTime());
                }
        );
        StatData data = new StatData();
        data.setProcessIDs(processIDs);
        data.setStartTimes(startTimes);
        data.setFinishTimes(finishTimes);
        data.setRequestedTimes(requestedTimes);
        data.setRunTimes(runTimes);
        stat.setData(data);
        return stat.computeStat();
    }

    protected double getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(double currentTime) {
        this.currentTime = currentTime;
    }

    protected void advanceTime(double timeDelta) {
        currentTime += timeDelta;
    }

    protected void addToFinished(Process process) {
        finished.add(process);
    }

    protected Process getNextProcess() {
        return futureProcesses.peek();
    }

    protected Process takeNextProcess() {
        return futureProcesses.poll();
    }

    public abstract String getName();
}
