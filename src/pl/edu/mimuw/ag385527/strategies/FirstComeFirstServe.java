package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.List;

public class FirstComeFirstServe extends PriorityQueueStrategy {

    private static final String NAME = "FCFS";

    public FirstComeFirstServe(List<ProcessDescription> processesToRun) {
        super(processesToRun, Process.DEFAULT_COMPARATOR);
    }

    @Override
    public String getName() {
        return NAME;
    }
}