package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.stats.Stat;

public interface Strategy {
    /**
    *  Przeprowadza symulację działania danej strategii
    */
    void run();

    /**
     * Oblicza statystyke na podstawie obiektu stat
     * @param stat - obiekt opisujący sposób obliczania danej statystyki
     * @param <T> - typ zwracanej statystyki
     * @return Wynik obliczenia statystyki
     */
    <T> T getStat(Stat<T> stat);

    /**
     * Zwraca nazwę strategii
     * @return Nazwa strategii
     */
    String getName();
}
