package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class ProcessorSharing extends AbstractStrategy {

    private static final String NAME = "PS";
    private final PriorityQueue<Process> processQueue;

    public ProcessorSharing(List<ProcessDescription> processesToRun) {
        super(processesToRun);
        processQueue = new PriorityQueue<>(1, Comparator.comparingDouble(Process::getRemainingTime));
    }

    @Override
    protected void handleProcesses() {
        Process soonestFinishingProcess = processQueue.peek();
        double timeUntilNextEvent;
        double processorTimePerProcess;
        double timeUntilSoonestProcessIsFinished = soonestFinishingProcess.getRemainingTime();
        if (getNextProcess() != null) {
            double timeUntilNextProcessAppears = getNextProcess().getStartTime() - getCurrentTime();
            if (timeUntilSoonestProcessIsFinished * processQueue.size() <= timeUntilNextProcessAppears) {
                processorTimePerProcess = timeUntilSoonestProcessIsFinished;
                timeUntilNextEvent = timeUntilSoonestProcessIsFinished * processQueue.size();
            } else {
                processorTimePerProcess = timeUntilNextProcessAppears / processQueue.size();
                timeUntilNextEvent = timeUntilNextProcessAppears;
            }
        } else {
            processorTimePerProcess = timeUntilSoonestProcessIsFinished;
            timeUntilNextEvent = timeUntilSoonestProcessIsFinished * processQueue.size();
        }

        advanceTime(timeUntilNextEvent);
        processQueue.forEach(p -> p.runFor(processorTimePerProcess, getCurrentTime(), timeUntilNextEvent));

        // jest to jedyna strategia z podanych w zadaniu,
        // w której więcej niż jeden proces może zakończyć się w tym samym czasie
        while (processQueue.peek() != null && processQueue.peek().isFinished()) {
            addToFinished(processQueue.poll());
        }
    }

    @Override
    protected boolean anyProcessLeft() {
        return processQueue.peek() != null || getNextProcess() != null;
    }

    @Override
    protected void skipWaitingTime() {
        if (processQueue.peek() == null && getNextProcess() != null) {
            setCurrentTime(getNextProcess().getStartTime());
        }
    }

    protected void addProcessesToQueue() {
        while (getNextProcess() != null && getNextProcess().getStartTime() == getCurrentTime()) {
            processQueue.add(takeNextProcess());
        }
    }

    @Override
    public String getName() {
        return NAME;
    }
}
