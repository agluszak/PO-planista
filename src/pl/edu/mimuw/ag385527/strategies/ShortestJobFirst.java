package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.Comparator;
import java.util.List;

public class ShortestJobFirst extends PriorityQueueStrategy {

    private static final String NAME = "SJF";
    public ShortestJobFirst(List<ProcessDescription> processesToRun) {
        super(processesToRun, Comparator.comparingDouble(Process::getRemainingTime).thenComparingInt(Process::getId));
    }

    @Override
    public String getName() {
        return NAME;
    }
}
