package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.List;
import java.util.Queue;

/**
 * Klasa bazowa dla takich strategii, które w danym momencie wykonują jedynie jeden proces
 */
public abstract class SingleProcessStrategy extends AbstractStrategy {

    protected final Queue<Process> processQueue;
    private Process currentProcess;

    public SingleProcessStrategy(List<ProcessDescription> processesToRun, Queue<Process> processQueue) {
        super(processesToRun);
        this.processQueue = processQueue;
    }

    @Override
    protected boolean anyProcessLeft() {
        return processQueue.peek() != null || getNextProcess() != null || currentProcess != null;
    }

    @Override
    protected void skipWaitingTime() {
        if (processQueue.peek() == null && currentProcess == null && getNextProcess() != null) {
            setCurrentTime(getNextProcess().getStartTime());
        }
    }

    protected void addProcessToQueue(Process process) {
        processQueue.add(process);
    }

    @Override
    protected void addProcessesToQueue() {
        while (getNextProcess() != null && getNextProcess().getStartTime() == getCurrentTime()) {
            addProcessToQueue(takeNextProcess());
        }
    }

    protected Process selectCurrentProcess() {
        if (getCurrentProcess() == null) {
            return processQueue.poll();
        } else {
            return getCurrentProcess();
        }
    }

    protected Process getCurrentProcess() {
        return currentProcess;
    }

    protected void setCurrentProcess(Process process) {
        currentProcess = process;
    }

    protected abstract void handleCurrentProcess();

    @Override
    public void handleProcesses() {
        setCurrentProcess(selectCurrentProcess());
        handleCurrentProcess();
    }

}
