package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.List;

public class ShortestRemainingTime extends ShortestJobFirst {

    private static final String NAME = "SRT";

    public ShortestRemainingTime(List<ProcessDescription> processesToRun) {
        super(processesToRun);
    }

    @Override
    protected void addProcessToQueue(Process process) {
        if (getCurrentProcess() != null && getCurrentProcess().getRemainingTime() > process.getRemainingTime()) {
            super.addProcessToQueue(getCurrentProcess());
            setCurrentProcess(process);
        } else {
            super.addProcessToQueue(process);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }
}
