package pl.edu.mimuw.ag385527.strategies;

import pl.edu.mimuw.ag385527.Process;
import pl.edu.mimuw.ag385527.ProcessDescription;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Klasa bazowa dla strategii korzystających z kolejki priorytetowej, która od razu zapewnia
 * właściwą kolejność wykonywania się procesów.
 */
public abstract class PriorityQueueStrategy extends SingleProcessStrategy {

    public PriorityQueueStrategy(List<ProcessDescription> processesToRun, Comparator<Process> comparator) {
        super(processesToRun, new PriorityQueue<>(1, comparator));
    }

    @Override
    protected void handleCurrentProcess() {
        double timeUntilNextEvent;
        double timeUntilCurrentProcessIsDone = getCurrentProcess().getRemainingTime();
        if (getNextProcess() != null) {
            double timeUntilNextProcessAppears = getNextProcess().getStartTime() - getCurrentTime();
            timeUntilNextEvent = Math.min(timeUntilCurrentProcessIsDone, timeUntilNextProcessAppears);
        } else {
            timeUntilNextEvent = timeUntilCurrentProcessIsDone;
        }
        advanceTime(timeUntilNextEvent);
        getCurrentProcess().runFor(timeUntilNextEvent, getCurrentTime(), timeUntilNextEvent);

        if (getCurrentProcess().isFinished()) {
            addToFinished(getCurrentProcess());
            setCurrentProcess(null);
        }
    }
}
