package pl.edu.mimuw.ag385527.input;

import pl.edu.mimuw.ag385527.ProcessDescription;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class InputParser {
    static private final String NUMBER_OF_PROCESSES_PATTERN = "(\\d+)";
    static private final String PROCESS_DESCRIPTION_PATTERN = "(\\d+) (\\d+)";
    static private final String NUMBER_OF_RR_VARIANTS_PATTERN = "(\\d+)";
    static private final String RR_VARIANTS_DESCRIPTION_PATTERN = "(?:\\d+ )*(?:\\d+)";

    static private final String ERROR_MESSAGE = "Błąd w wierszu";
    static private final String SEPARATOR = " ";
    static private final String COLON = ":";

    static private final String NUMBER_OF_PROCESSES_ERROR = "Błąd przy parsowaniu liczby procesów w serii.";
    static private final String PROCESS_DESCRIPTION_ERROR = "Błąd przy parsowaniu opisu procesu.";
    static private final String NUMBER_OF_RR_VARIANTS_ERROR = "Błąd przy parsowaniu liczby wariantów strategii RR.";
    static private final String RR_VARIANTS_DESCRIPTION_ERROR = "Błąd przy parsowaniu opisu strategii RR.";
    static private final String UNEXPECTED_END_OF_FILE_ERROR = "Nieoczekiwany koniec pliku.";
    static private final String NO_END_OF_FILE_ERROR = "Plik zawiera dodatkowe, niepotrzebne dane.";
    static private final String NUMBER_FORMAT_ERROR = "Błąd przy parsowaniu liczby.";
    static private final String REQUESTED_TIME_LESS_THAN_ONE_ERROR = "Zapotrzebowanie jest mniejsze niż 1.";
    static private final String NUMBER_OF_PROCESSES_LESS_THAN_ONE_ERROR = "Ilość procesów jest mniejsza niż 1";
    static private final String NUMBER_OF_RR_VARIANTS_LESS_THAN_ONE_ERROR = "Ilość wariantów strategii RR jest mniejsza niż 1";
    static private final String RR_VARIANT_QUANTUM_LESS_THAN_ONE = "Kwant czasu w strategii RR mniejszy niż 1";
    static private final String NO_PROCESS_STARTING_AT_ZERO_ERROR = "Brak procesu rozpoczynającego się o czasie 0.";
    static private final String UNKNOWN_ERROR = "Nieznany błąd.";

    private String error;
    private boolean valid;
    private Reader reader;
    private int currentLineNumber;
    private int numberOfProcesses;
    private List<ProcessDescription> processDescriptions;
    private int numberOfRRVariants;
    private List<Integer> rrVariants;

    public InputParser(Reader reader) {
        this.reader = reader;
        this.valid = false;
        this.error = "";
        this.currentLineNumber = 1;
        this.numberOfProcesses = 0;
        this.processDescriptions = new ArrayList<>();
        this.numberOfRRVariants = 0;
        this.rrVariants = new ArrayList<>();
    }

    private void setError(String error) {
        this.error = ERROR_MESSAGE + SEPARATOR + String.valueOf(currentLineNumber) + COLON + SEPARATOR + error;
        this.valid = false;
    }

    private String getNextLine(BufferedReader bufferedReader) throws IOException, UnexpectedEndOfFileException {
        String nextLine = bufferedReader.readLine();
        if (nextLine != null) {
            return nextLine;
        } else {
            throw new UnexpectedEndOfFileException();
        }
    }

    public boolean isValid() {
        return valid;
    }

    public String getErrorMessage() {
        return error;
    }

    public void parseInput() {
        try (BufferedReader bufferedReader = new BufferedReader(reader)) {
            String currentLine = getNextLine(bufferedReader);
            // Wczytywanie liczby procesów
            if (currentLine.matches(NUMBER_OF_PROCESSES_PATTERN)) {
                numberOfProcesses = Integer.parseInt(currentLine);
                if (numberOfProcesses < 1) {
                    setError(NUMBER_OF_PROCESSES_LESS_THAN_ONE_ERROR);
                    return;
                }
            } else {
                setError(NUMBER_OF_PROCESSES_ERROR);
                return;
            }
            // Wczytywanie opisów kolejnych procesów
            boolean foundProcessStartingAtZero = false;
            for (int currentProcessNumber = 1; currentProcessNumber <= numberOfProcesses; currentProcessNumber++) {
                currentLineNumber++;
                currentLine = getNextLine(bufferedReader);
                if (currentLine.matches(PROCESS_DESCRIPTION_PATTERN)) {
                    String[] descriptionParams = currentLine.split(SEPARATOR);
                    int start = Integer.parseInt(descriptionParams[0]);
                    if (start == 0) {
                        foundProcessStartingAtZero = true;
                    }
                    int timeRequested = Integer.parseInt(descriptionParams[1]);
                    if (timeRequested < 1) {
                        setError(REQUESTED_TIME_LESS_THAN_ONE_ERROR);
                        return;
                    }
                    ProcessDescription description = new ProcessDescription(currentProcessNumber, start, timeRequested);
                    processDescriptions.add(description);
                } else {
                    setError(PROCESS_DESCRIPTION_ERROR);
                    return;
                }
            }
            if (!foundProcessStartingAtZero) {
                setError(NO_PROCESS_STARTING_AT_ZERO_ERROR);
                return;
            }
            currentLineNumber++;
            currentLine = getNextLine(bufferedReader);
            // Wczytywanie liczby wariantów strategii RR
            if (currentLine.matches(NUMBER_OF_RR_VARIANTS_PATTERN)) {
                numberOfRRVariants = Integer.parseInt(currentLine);
                if (numberOfRRVariants < 1) {
                    setError(NUMBER_OF_RR_VARIANTS_LESS_THAN_ONE_ERROR);
                    return;
                }
            } else {
                setError(NUMBER_OF_RR_VARIANTS_ERROR);
                return;
            }
            currentLineNumber++;
            currentLine = getNextLine(bufferedReader);
            // Wczytywanie wariantów strategii RR
            if (currentLine.matches(RR_VARIANTS_DESCRIPTION_PATTERN)) {
                String[] variants = currentLine.split(SEPARATOR);
                for (int i = 0; i < numberOfRRVariants; i++) {
                    int rrQuant = Integer.valueOf(variants[i]);
                    if (rrQuant < 1) {
                        setError(RR_VARIANT_QUANTUM_LESS_THAN_ONE);
                        return;
                    }
                    rrVariants.add(rrQuant);
                }
            } else {
                setError(RR_VARIANTS_DESCRIPTION_ERROR);
                return;
            }
            if (bufferedReader.readLine() != null) {
                setError(NO_END_OF_FILE_ERROR);
                return;
            }
            valid = true;
        } catch (IOException e) {
            setError(UNKNOWN_ERROR);
        } catch (NumberFormatException e) {
            setError(NUMBER_FORMAT_ERROR);
        } catch (UnexpectedEndOfFileException e) {
            setError(UNEXPECTED_END_OF_FILE_ERROR);
        }
    }

    public List<ProcessDescription> getProcessDescriptions() {
        return processDescriptions;
    }

    public List<Integer> getRrVariants() {
        return rrVariants;
    }

}
