package pl.edu.mimuw.ag385527.stats;

import java.util.Locale;

public class ProcessResult {
    private final int id;
    private final int startTime;
    private final double finishTime;

    public ProcessResult(int id, int startTime, double finishTime) {
        this.id = id;
        this.startTime = startTime;
        this.finishTime = finishTime;
    }

    public int getId() {
        return id;
    }

    public int getStartTime() {
        return startTime;
    }

    public double getFinishTime() {
        return finishTime;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "[%d %d %.2f]", id, startTime, finishTime);
    }
}
