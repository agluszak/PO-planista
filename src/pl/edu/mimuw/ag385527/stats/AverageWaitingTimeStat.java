package pl.edu.mimuw.ag385527.stats;

public class AverageWaitingTimeStat extends Stat<Double> {
    @Override
    public Double computeStat() {
        double sum = 0;
        for (int id : data.getProcessIDs()) {
            double waitingTime = data.getFinishTimes().get(id) -
                    data.getStartTimes().get(id) -
                    data.getRunTimes().get(id);
            sum += waitingTime;
        }
        return sum / data.getProcessIDs().size();
    }
}
