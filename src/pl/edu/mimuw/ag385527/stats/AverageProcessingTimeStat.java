package pl.edu.mimuw.ag385527.stats;

public class AverageProcessingTimeStat extends Stat<Double> {
    @Override
    public Double computeStat() {
        double sum = 0;
        for (int id : data.getProcessIDs()) {
            double processingTime = data.getFinishTimes().get(id) -
                    data.getStartTimes().get(id);
            sum += processingTime;
        }
        return sum / data.getProcessIDs().size();
    }
}
