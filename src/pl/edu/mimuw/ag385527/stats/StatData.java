package pl.edu.mimuw.ag385527.stats;

import java.util.List;
import java.util.Map;

/**
 * Klasa zawierająca dane potrzebne do obliczenia "kryterium"
 */
public class StatData {
    private List<Integer> processIDs;
    private Map<Integer, Integer> startTimes;
    private Map<Integer, Double> finishTimes;
    private Map<Integer, Double> runTimes;
    private Map<Integer, Integer> requestedTimes;

    public Map<Integer, Integer> getRequestedTimes() {
        return requestedTimes;
    }

    public void setRequestedTimes(Map<Integer, Integer> requestedTimes) {
        this.requestedTimes = requestedTimes;
    }

    public List<Integer> getProcessIDs() {
        return processIDs;
    }

    public void setProcessIDs(List<Integer> processIDs) {
        this.processIDs = processIDs;
    }

    public Map<Integer, Integer> getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(Map<Integer, Integer> startTimes) {
        this.startTimes = startTimes;
    }

    public Map<Integer, Double> getFinishTimes() {
        return finishTimes;
    }

    public void setFinishTimes(Map<Integer, Double> finishTimes) {
        this.finishTimes = finishTimes;
    }

    public Map<Integer, Double> getRunTimes() {
        return runTimes;
    }

    public void setRunTimes(Map<Integer, Double> runTimes) {
        this.runTimes = runTimes;
    }
}
