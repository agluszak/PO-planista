package pl.edu.mimuw.ag385527.stats;

/**
 * Klasa reprezentująca sposób na obliczenie "kryterium"
 * @param <T> Typ obliczanego "kryterium"
 */
public abstract class Stat<T> {
    protected StatData data;

    public void setData(StatData data) {
        this.data = data;
    }

    abstract public T computeStat();
}
