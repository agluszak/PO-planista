package pl.edu.mimuw.ag385527.stats;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Klasa opisująca sposób na obliczenie napisu postaci
 * [1 0 10.00][2 0 39.00][3 0 42.00][4 0 49.00][5 0 61.00]
 * Nie jest to może kryterium w ścisłym tego słowa znaczeniu,
 * natomiast jak najbardziej jest pewną statystyką, mówiącą o tym jak działa dana strategia.
 * Do obliczenia takiego napisu zdecydowałem się użyć klasy Stat
 * aby zademonstrować jak ogólne "statystyki" czy "kryteria" pozwala ona reprezentować.
 */
public class StrategyOutputStat extends Stat<String> {

    @Override
    public String computeStat() {
        List<ProcessResult> list = new ArrayList<>();
        for (int id : data.getProcessIDs()) {
            int startTime = data.getStartTimes().get(id);
            double finishTime = data.getFinishTimes().get(id);
            ProcessResult result = new ProcessResult(id, startTime, finishTime);
            list.add(result);
        }
        list.sort(Comparator.comparingDouble(ProcessResult::getFinishTime).thenComparingInt(ProcessResult::getId));
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach(result -> stringBuilder.append(result.toString()));
        return stringBuilder.toString();
    }
}
