package pl.edu.mimuw.ag385527;

import java.util.Comparator;

/**
 * Klasa reprezentująca proces na którym dokonywane są obliczenia
 */
public class Process {
    public static final Comparator<Process> DEFAULT_COMPARATOR =
            Comparator.comparingInt(Process::getStartTime).thenComparingInt(Process::getId);

    private int requestedTime;
    private boolean isFinished;
    /**
     * Czas, który procesor spędził na obliczaniu danego procesu (istotne w przypadku strategii ProcessorSharing)
     */
    private double timeInProcessing;
    private int startTime;
    private double finishTime;
    /**
     * Czas potrzebny do skończenia obliczeń
     */
    private double remainingTime;
    private int id;

    public Process(ProcessDescription description) {
        this.isFinished = false;
        this.finishTime = 0;
        this.timeInProcessing = 0;
        this.requestedTime = description.getRequestedTime();
        this.remainingTime = this.requestedTime;
        this.startTime = description.getStart();
        this.id = description.getId();
    }

    public int getStartTime() {
        return startTime;
    }

    public int getId() {
        return id;
    }

    public void runFor(double timeAmount, double endTime, double timeInProcessing) {
        this.remainingTime -= timeAmount;
        this.timeInProcessing += timeInProcessing;
        if (this.remainingTime <= 0) {
            this.isFinished = true;
            this.finishTime = endTime;
            this.remainingTime = 0;
        }
    }

    public int getRequestedTime() {
        return requestedTime;
    }

    public double getTimeInProcessing() {
        return timeInProcessing;
    }

    public double getRemainingTime() {
        return remainingTime;
    }

    public double getFinishTime() {
        return finishTime;
    }

    public boolean isFinished() {
        return isFinished;
    }
}
