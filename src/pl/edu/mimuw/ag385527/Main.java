package pl.edu.mimuw.ag385527;

import pl.edu.mimuw.ag385527.input.InputParser;
import pl.edu.mimuw.ag385527.stats.AverageProcessingTimeStat;
import pl.edu.mimuw.ag385527.stats.AverageWaitingTimeStat;
import pl.edu.mimuw.ag385527.stats.Stat;
import pl.edu.mimuw.ag385527.stats.StrategyOutputStat;
import pl.edu.mimuw.ag385527.strategies.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Klasa główna programu "Planista"
 */
public class Main {

    public static void main(String[] args) {
        // sprawdzamy na który ze sposobów przekazania danych wejściowych zdecydował się użytkownik
        Reader reader;
        if (args.length == 1) {
            try {
                reader = new FileReader(args[0]);
            } catch (FileNotFoundException e) {
                System.err.println("Plik z danymi nie jest dostępny.");
                return;
            }
        } else if (args.length == 0) {
            reader = new InputStreamReader(System.in);
        } else {
            System.err.println("Nieprawidłowe parametry wywołania programu.");
            return;
        }

        // wczytujemy i parsujemy dane
        BufferedReader br = new BufferedReader(reader);
        InputParser parser = new InputParser(br);
        parser.parseInput();
        if (!parser.isValid()) {
            System.err.println(parser.getErrorMessage());
            return;
        }

        List<ProcessDescription> descriptions = parser.getProcessDescriptions();
        List<Integer> rrVariants = parser.getRrVariants();

        Stat<Double> averageProcessingTimeStat = new AverageProcessingTimeStat();
        Stat<Double> averageWaitingTimeStat = new AverageWaitingTimeStat();
        Stat<String> strategyOutputStat = new StrategyOutputStat();

        List<Strategy> strategyList = new ArrayList<>();
        strategyList.add(new FirstComeFirstServe(descriptions));
        strategyList.add(new ShortestJobFirst(descriptions));
        strategyList.add(new ShortestRemainingTime(descriptions));
        strategyList.add(new ProcessorSharing(descriptions));
        rrVariants.forEach(quantum -> strategyList.add(new RobinRound(descriptions, quantum)));

        // testujemy strategie
        strategyList.forEach(strategy -> {
            strategy.run();
            System.out.format("Strategia: %s\n", strategy.getName());
            System.out.format("%s\n", strategy.getStat(strategyOutputStat));
            System.out.format(Locale.US, "Średni czas obrotu: %.2f\n",
                    strategy.getStat(averageProcessingTimeStat));
            // abs jest potrzebny, bo czasami dla strategii PS wychodzi -0.00
            System.out.format(Locale.US, "Średni czas oczekiwania: %.2f\n",
                    Math.abs(strategy.getStat(averageWaitingTimeStat)));
            System.out.format("\n");
        });

    }
}
